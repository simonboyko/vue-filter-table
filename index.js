import Vue from 'vue';
import App from './App.vue';
import Vuex from 'vuex';
import st from './store';

Vue.use(Vuex);

const store = new Vuex.Store({
  state: {
    origInfo: st,
  },
  getters: {
    getOrigInfo: state => {
      return state.origInfo.list;
    }
  }
});

new Vue({
  el: '#app',
  store,
  render: h => h(App)
});


